
import io
import os
import base64
import pandas as pd
import pandas_gbq
from google.cloud import bigquery, storage
from google.oauth2 import service_account
import warnings
warnings.filterwarnings('ignore')



def get_SA_credentials():
    # Service Account Credentials
    service_account_key_path = os.environ['GCP_SERVICE_ACCOUNT_KEY_PATH']

    # prepare credentials
    credentials = service_account.Credentials.from_service_account_file(
        service_account_key_path, scopes=["https://www.googleapis.com/auth/cloud-platform"],
    )

    # create BigQuery client
    #client = bigquery.Client(credentials=credentials, project=credentials.project_id)

    return credentials#, client

def BigQuery_to_DF(sql_query, job_config=None):
    # get Service Account Credentials
    credentials = get_SA_credentials()

    # run query
    if job_config is None:
        df = pd.read_gbq(sql_query, credentials=credentials)
    else:
        df = pd.read_gbq(sql_query, credentials=credentials, configuration=job_config)

    return df

def DF_to_BigQuery(df, schema, bq_dataset_name, bq_table_name, mode='append'):
    # get Service Account Credentials
    credentials = get_SA_credentials()

    # prepare table name
    table_id = bq_dataset_name + "." + bq_table_name

    # load DataFrame to BigQuery
    pandas_gbq.to_gbq(df, table_id, credentials=credentials, if_exists=mode, table_schema=schema)

def connect_to_Storage():
    # get Service Account Credentials
    credentials = get_SA_credentials()

    # create Google Cloud Storage client
    client = storage.Client(
        credentials=credentials, 
        project=credentials.project_id
    )

    return client

def Object_to_Storage(bucket_name, destination_file_path, destination_filename, source_object, content_type):
    # connect to Storage
    client = connect_to_Storage()

    # load bucket
    bucket = client.bucket(bucket_name)

    # prepare Destination Blob
    blob = bucket.blob(destination_file_path+destination_filename)

    # prepare object & upload
    if content_type=='image/png':
        buf = io.BytesIO()
        source_object.savefig(buf, format='png')

        # upload buffer contents to gcs
        blob.upload_from_string(buf.getvalue(), content_type=content_type)
        buf.close()

    elif content_type=='text/csv':
        blob.upload_from_file(source_object, content_type=content_type)
    elif content_type=='text/html':
        blob.upload_from_string(source_object, content_type=content_type)
    elif content_type=='text':
        blob.upload_from_filename(source_object, content_type=content_type)

def File_to_Storage(bucket_name, destination_file_path, source_file_path):
    # connect to Storage
    client = connect_to_Storage()

    # load bucket
    bucket = client.bucket(bucket_name)

    # prepare Destination Blob
    blob = bucket.blob(destination_file_path)

    # upload from Local Source path to Storage Destination path
    blob.upload_from_filename(source_file_path)

def Storage_to_File(bucket_name, source_file_path, destination_file_path):
    # connect to Storage
    client = connect_to_Storage()

    # load bucket
    bucket = client.bucket(bucket_name)

    # prepare Destination Blob
    blob = bucket.blob(source_file_path)

    # download from Storage Destination path to Local Source path
    blob.download_to_filename(destination_file_path)

def Storage_to_DF(bucket_name, source_file_path):
    # connect to Storage
    client = connect_to_Storage()

    # load bucket
    bucket = client.bucket(bucket_name)

    # prepare Destination Blob
    blob = bucket.blob(source_file_path)

    # download from Storage Destination path to DataFrame
    data = blob.download_as_string()
    df = pd.read_csv(io.StringIO(data.decode('utf-8')))

    return df

def get_sql(sql_file_name):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    sql_file_path = os.path.join(dir_path, 'sql', sql_file_name)
    
    with open(sql_file_path, 'r') as f:
        sql_query = f.read()

    return sql_query