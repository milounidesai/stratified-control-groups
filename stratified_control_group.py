
# import modules
import os
import logging
import sys
import argparse
import datetime as dt
from dateutil import relativedelta
import numpy as np
import pandas as pd
from IPython.display import display

from google.cloud import bigquery
from google.oauth2 import service_account

from utils_python_gcp import *
from utils_stratification import *

if __name__ == '__main__':
    logger = logging.getLogger("airflow.task")
    logger.setLevel(logging.INFO)
    ch = logging.StreamHandler(sys.stdout)
    logger.addHandler(ch)

    logger.info("#################################################################################")
    logger.info("##################################### BEGIN #####################################")
    logger.info("#################################################################################\n")

    starttime = dt.datetime.now()
    logger.info("Start: ", starttime)

    # determine period
    nextmonth = dt.date.today() + relativedelta(months=1)
    period = nextmonth.year*100+nextmonth.month
    logger.info("Period: ", period)

    # initialize common variables
    add_extra_rows_randomly = False
    save_to_BQ = True
    save_to_GCS = True
    is_testing = False
    column = 'avg_spend_L3M'

    storage_bucket = 'fpretail-data-cloud-analytics'
    base_storage_path = 'marketing-analytics/Milouni/Stratified_Control_Groups/'+str(period)
    results_filename = 'Result - Summary Report.csv'

    # initialize DCC variables
    dcc_target_criteria = '(DCC-CG) EDM contactable & Belong to O2O Base in 2021 YTD & Have valid CSN, Online_ID, Master_ID, Email_ID & Not in any CG in L12M (stratification on L3M avg. spend TD based on active months)'
    dcc_control_size = 0.10
    dcc_base_query_path = 'query_dcc_base.sql'
    dcc_bq_table_name = 'EDM_DCC_control_group'
    dcc_storage_path = base_storage_path+'/DCC_Control_Group/'

    # initialize Universal variables
    universal_target_criteria = '(U-CG) EDM contactable & Have valid CSN, Email_ID & Not in any CG in L12M (stratification on L3M avg. spend TD based on active months)'
    universal_control_size = 25000
    universal_base_query_path = 'query_universal_base.sql'
    universal_bq_table_name = 'EDM_universal_control_group'
    universal_storage_path = base_storage_path+'/Universal_Control_Group/'

    if is_testing == True:
        dcc_bq_table_name = dcc_bq_table_name + '_testing'
        dcc_storage_path = dcc_storage_path + 'testing/'

        universal_bq_table_name = universal_bq_table_name + '_testing'
        universal_storage_path = universal_storage_path + 'testing/'


    logger.info("\n############################### DCC CONTROL GROUP ###############################\n")

    logger.info("\nGetting Customer Base...")
    dcc_population = get_base(dcc_base_query_path)

    logger.info("\nPerforming Stratification...")
    dcc_population, dcc_test, dcc_control = get_control_group(dcc_population, 
                                                                    dcc_control_size, 
                                                                    dcc_bq_table_name, 
                                                                    storage_bucket, 
                                                                    dcc_storage_path, 
                                                                    add_extra_rows_randomly, 
                                                                    save_to_BQ, 
                                                                    save_to_GCS)

    logger.info("\nLogging Results...")
    log_results(dcc_target_criteria, dcc_population, dcc_test, dcc_control, column, storage_bucket, dcc_storage_path, results_filename)

    logger.info("\n############################ UNIVERSAL CONTROL GROUP ############################\n")

    logger.info("\nGetting Customer Base...")
    universal_population = get_base(universal_base_query_path)
    #universal_population = universal_population[~universal_population['csn'].isin(omnichannel_control['csn'])]

    logger.info("\nPerforming Stratification...")
    universal_population, universal_test, universal_control = get_control_group(universal_population, 
                                                                                universal_control_size, 
                                                                                universal_bq_table_name, 
                                                                                storage_bucket, 
                                                                                universal_storage_path, 
                                                                                add_extra_rows_randomly, 
                                                                                save_to_BQ, 
                                                                                save_to_GCS)

    logger.info("\nLogging Results...")
    log_results(universal_target_criteria, universal_population, universal_test, universal_control, column, storage_bucket, universal_storage_path, results_filename)



    endtime = dt.datetime.now()
    logger.info("End: ", endtime)
    logger.info("Duration: ", endtime - starttime)

    logger.info("\n#################################################################################")
    logger.info("###################################### END ######################################")
    logger.info("#################################################################################")

    logger.info("Job Complete.")
