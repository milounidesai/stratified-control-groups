
import logging
import sys
import argparse
import os
from os import listdir
from os.path import isfile, join
import datetime as dt
from dateutil.relativedelta import relativedelta
import pandas as pd
import numpy as np
import pandas.core.algorithms as algos
from sklearn.model_selection import train_test_split
#from verstack.stratified_continuous_split import scsplit
from IPython.display import display

from utils_python_gcp import *

logger = logging.getLogger("airflow.task")
logger.setLevel(logging.INFO)
ch = logging.StreamHandler(sys.stdout)
logger.addHandler(ch)

def get_sql(sql_file_name):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    sql_file_path = os.path.join(dir_path, 'sql', sql_file_name)
    
    with open(sql_file_path, 'r') as f:
        sql_query = f.read()

    return sql_query

def get_base(sql_file_name):
    sql_query = get_sql(sql_file_name)
    results = BigQuery_to_DF(sql_query)
    results['avg_spend_L3M'] = results['avg_spend_L3M'].astype('int')

    return results

def stratification(df, column, size, add_extra_rows_randomly):
    # number of bins to split target variable into
    count_bins = 20

    # get control size
    if size > 0 and size < 1:
        control_size = int(size*len(df))
    elif size > 1:
        control_size = size
    else:
        logger.info("Invalid Control Size.")
    logger.info("Control Size Required: ", control_size)
    
    # stratify based on datatype of column
    if df.dtypes[column] == 'object':
        pop, control_df = train_test_split(df, test_size=control_size, random_state=2021, stratify=df[[column]])
    elif df.dtypes[column] == 'int64' or df.dtypes[column] == 'int32' or df.dtypes[column] == 'float64':         
        df['rank'] = df[column].rank(method='first')
        df['bins'] = pd.qcut(df['rank'].values, count_bins).codes
        pop, control_df = train_test_split(df, test_size=control_size, random_state=2021, stratify=df[['bins']])

    # if control DataFrame is smaller than required control size, add more data by random sampling
    if add_extra_rows_randomly == True:
        if len(control_df) < control_size:
            df = df[~df['csn'].isin(control_df['csn'])]
            temp_control = df.control(n=control_size-len(control_df))
            control_df = control_df.append(temp_control)
    
    test_df = df[~df['csn'].isin(control_df['csn'])]

    return df, test_df, control_df  

def get_control_group(base_df, size, bq_table_name, storage_bucket, storage_path, add_extra_rows_randomly, save_to_BQ, save_to_GCS):
    logger.info('Population Size:', len(base_df))

    # perform stratification of required columns
    population_data, test_group, control_group = stratification(base_df, column='avg_spend_L3M', size=size, add_extra_rows_randomly=add_extra_rows_randomly)
    logger.info("Final Control Size:", len(control_group))

    # save Universal Control Group into BigQuery and locally
    next_month = dt.datetime.now() + relativedelta(months=+1)

    control_group['updated_date'] = dt.datetime.today().strftime("%Y-%m-%d")
    control_group['period'] = next_month.year*100 + next_month.month
    
    schema = [{'name': 'updated_date', 'type': 'STRING'}, 
                {'name': 'period', 'type': 'INT64'},
                {'name': 'csn', 'type': 'STRING'}, 
                {'name': 'master_id', 'type': 'STRING'}, 
                {'name': 'online_id', 'type': 'STRING'}, 
                {'name':'hashed_email', 'type':'INT64'}]
    
    #upload to GCS
    if save_to_GCS == True:
        df_file = io.StringIO()
        base_df.to_csv(df_file)
        df_file.seek(0)
        Object_to_Storage(bucket_name=storage_bucket, destination_file_path=storage_path, destination_filename='List - Entire_Population.csv', source_object=df_file, content_type='text/csv')

        df_file = io.StringIO()
        test_group.to_csv(df_file)
        df_file.seek(0)
        Object_to_Storage(bucket_name=storage_bucket, destination_file_path=storage_path, destination_filename='List - Test_Group.csv', source_object=df_file, content_type='text/csv')
        
        df_file = io.StringIO()
        control_group.to_csv(df_file)
        df_file.seek(0)
        Object_to_Storage(bucket_name=storage_bucket, destination_file_path=storage_path, destination_filename='List - Control_Group.csv', source_object=df_file, content_type='text/csv')
    
    # save to BigQuery
    if save_to_BQ == True:
        DF_to_BigQuery(control_group[['updated_date', 'period', 'csn', 'master_id', 'online_id', 'hashed_email']], schema=schema, bq_dataset_name='fp_cam', bq_table_name=bq_table_name, mode='append')

    return population_data, test_group, control_group

def log_results(target_criteria, population, test, control, column, bucketname, destination_file_path, destination_file_name):

    population.name = 'population'
    control.name = 'control'
    test.name = 'test'

    logs_df = pd.DataFrame(columns=['metric', 'value1', 'value2', 'value3', 'value4'])
    rows = []

    rows.append({'metric':'Target Criteria', 'value1':None, 'value2':None, 'value3':None, 'value4':None})
    rows.append({'metric':target_criteria, 'value1':None, 'value2':None, 'value3':None, 'value4':None})

    for row in rows:
        logs_df = logs_df.append(row, ignore_index=True)

    for df in [population, control, test]:
        rows = []
        if df.name == 'population':
            rows.append({'metric':'Entire Population Details:', 'value1':None, 'value2':None, 'value3':None, 'value4':None})
        elif df.name == 'control':
            rows.append({'metric':'Control Group Details:', 'value1':None, 'value2':None, 'value3':None, 'value4':None})
        elif df.name == 'test':
            rows.append({'metric':'Test Group Details:', 'value1':None, 'value2':None, 'value3':None, 'value4':None})

        rows.append({'metric':'Total Members:', 'value1':len(df), 'value2':None, 'value3':None, 'value4':None})
        rows.append({'metric':'Avg Spend L3M:', 'value1':round(np.mean(df[column]), 2), 'value2':None, 'value3':None, 'value4':None})
        rows.append({'metric':'Distribution:', 'value1':None, 'value2':None, 'value3':None, 'value4':None})
        

        dist_df = pd.DataFrame(df[['csn', column, 'rank', 'bins']].groupby('bins').agg(
                        count_members = pd.NamedAgg(column='csn', aggfunc=pd.Series.nunique),
                        min_spend = pd.NamedAgg(column=column, aggfunc=min),
                        max_spend = pd.NamedAgg(column=column, aggfunc=max),
                        avg_spend = pd.NamedAgg(column=column, aggfunc=np.mean)
        )).reset_index()
        dist_df.columns = logs_df.columns
        
        rows.append({'metric':'Spend Bins', 'value1':'# Members', 'value2':'Min Spend', 'value3':'Max Spend', 'value4':'Avg Spend'})
        
        for row in rows:
            logs_df = logs_df.append(row, ignore_index=True)
        logs_df = logs_df.append(dist_df, ignore_index=True)

    df_file = io.StringIO()
    logs_df.to_csv(df_file)
    df_file.seek(0)

    Object_to_Storage(bucket_name=bucketname, destination_file_path=destination_file_path, destination_filename=destination_file_name, source_object=df_file, content_type='text/csv')

