
DECLARE given_date      DATE        DEFAULT current_date();
DECLARE TY              INT64       DEFAULT extract(year from given_date);
DECLARE last_mth        INT64       DEFAULT extract(month from date_sub(DATE_TRUNC(given_date, month), INTERVAL 1 month));
DECLARE TY_L0M          INT64       DEFAULT extract(year from date_sub(DATE_TRUNC(given_date, month), INTERVAL 0 month))*100+extract(month from date_sub(DATE_TRUNC(given_date, month), INTERVAL 0 month));
DECLARE TY_L1M          INT64       DEFAULT extract(year from date_sub(DATE_TRUNC(given_date, month), INTERVAL 1 month))*100+extract(month from date_sub(DATE_TRUNC(given_date, month), INTERVAL 1 month));
DECLARE TY_L2M          INT64       DEFAULT extract(year from date_sub(DATE_TRUNC(given_date, month), INTERVAL 2 month))*100+extract(month from date_sub(DATE_TRUNC(given_date, month), INTERVAL 2 month));

with 
    base as (
        select distinct
        a.link_id as csn,
        a.master_id,
        a.online_id,
        null as hashed_email
        from `ne-fprt-data-cloud-production`.fp.fp_customer_master as a
        left join `ne-link-data-cloud-production`.edw.dim_client_master as b on b.csn = a.link_id
        where a.link_id is not null and a.master_id is not null and a.online_id is not null
            and b.active = 'A' and a.online_comms_by_email='Y'
    ),
    O2O_base as (
        select distinct 
            cast(customer_id as STRING) as online_id,
        from `fairprice-bigquery.fpon_cdm.fpon_cdm_sales_head`
        where extract(year from date_of_order) = TY
            --and extract(month from date_of_order) = last_mth
            and order_status NOT IN ('PENDING', 'CANCELLED')
            and order_type IN ('OFFLINE')
            and customer_id IS NOT NULL
        group by 1
    ),
    spend as (
        select
            c.link_id as csn,
            sum(a.sale_net_val)/1.07 as spend,
            count(distinct extract(month from a.business_date)) as count_months
        from `ne-fprt-data-cloud-production`.fp.fp_sale_line as a
        left join `ne-fprt-data-cloud-production`.fp.fp_sale_bridge_master as b on b.business_date=a.business_date and b.store_code=a.store_code and b.invoice_no=a.invoice_no and b.till_code=a.till_code
        left join `ne-fprt-data-cloud-production`.fp.fp_customer_master as c on c.master_id=b.master_id
        left join `ne-fprt-data-cloud-production`.fp.fp_store as d on d.store_code=a.store_code
        where extract(year from a.business_date)*100+extract(month from a.business_date) in (TY_L0M, TY_L1M, TY_L2M)
            and a.sale_net_val > 0
        group by c.link_id
    ),
    final_data as (
        select
            a.*,
            coalesce(round(b.spend/count_months), 0) as avg_spend_L3M
        from base as a
        left join spend as b on b.csn = a.csn
    )
select * from final_data 
where cast(online_id as STRING) in (select distinct online_id from O2O_base)
    and csn not in (select distinct csn from `ne-fprt-data-cloud-production`.fp_cam.EDM_universal_control_group where extract(year from current_date())*100+extract(month from current_date()) - period < 12)
    and csn not in (select distinct csn from `ne-fprt-data-cloud-production`.fp_cam.EDM_DCC_control_group where extract(year from current_date())*100+extract(month from current_date()) - period < 12)
;
